# README #

This repository includes the AWS CloudFormation support files for creating an IAM role and permissions that grant Yotascale the ability to read tenant account info.

### Purpose ###

* AWS CloudFormation Template for IAM Permissions
* Version: 1.0

### Setup ###

* The core CF template should be pushed to a public S3 bucket for new tenants to use, usually located at:
	* URL https://yotascale-onboarding.s3.amazonaws.com/YotascaleManagement.yaml
	* S3 URL: s3://yotascale-onboarding/YotascaleManagement.yaml
* An AWS user for the tenant would enter the "S3 URL" into AWS Cloud Formation to get started and define their "stack". AWS user would then deploy the stack to affect the IAM role and permission changes.

### Owners ###

* Jeff Harris, jeff.harris.yotascale.com
* Basil Hashem, basil@yotascale.com
